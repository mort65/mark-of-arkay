$General	General 
$Extra	Extra
$Aftermath	Aftermath
$Debug	Debug
$Settings	Settings
$Random	Random
$Nearby	Nearby
$Info	Information
$Whiterun	Whiterun
$Falkreath	Falkreath
$Markarth	Markarth
$Riften	Riften
$Solitude	Solitude
$Windhelm	Windhelm
$Winterhold	Winterhold
$RavenRock	Raven Rock
$Custom	Custom
$External	External
$LastSleepLocation	Slept Location
$Disabled	Disabled
$Unknown	Unknown
$History	History
$Bleedouts	Bleedouts
$Revivals	Revivals
$Respawns	Respawns
$Are_You_Sure	Are You Sure?
$mrt_MarkofArkay_HEAD_Locked	Mod is locked
$mrt_MarkofArkay_RevivalEnabled	Enable Revival
$mrt_MarkofArkay_MarkOfArkayRevivalEnabled	Enable Arkay Mark Revival
$mrt_MarkofArkay_GSoulGemRevivalEnabled	Enable Grand Soulgem Revival
$mrt_MarkofArkay_BSoulGemRevivalEnabled	Enable Black Soulgem Revival
$mrt_MarkofArkay_DragonSoulRevivalEnabled	Enable Dragon Soul Revival
$mrt_MarkofArkay_GoldRevivalEnabled	Enable Septim Revival
$mrt_MarkofArkay_MarkSlider_1	Arkay Marks Used
$mrt_MarkofArkay_MarkSlider_2	{0} Arkay Marks
$mrt_MarkofArkay_GSoulGemSlider_1	Grand Soulgems Used
$mrt_MarkofArkay_GSoulGemSlider_2	{0} Grand Soulgems
$mrt_MarkofArkay_BSoulGemSlider_1	Black Soulgems Used
$mrt_MarkofArkay_BSoulGemSlider_2	{0} Black Soulgems
$mrt_MarkofArkay_DragonSoulSlider_1	Dragon Souls Used
$mrt_MarkofArkay_DragonSoulSlider_2	{0} Dragon Souls
$mrt_MarkofArkay_GoldSlider_1	Septims Used
$mrt_MarkofArkay_GoldSlider_2	{0} Septims
$mrt_MarkofArkay_RecoveryTime_1	Recovery Delay
$mrt_MarkofArkay_RecoveryTime_2	{0} Seconds
$mrt_MarkofArkay_BleedoutTime_1	Bleedout Delay
$mrt_MarkofArkay_LootChanceSlider_1	Arkay Mark Chance
$mrt_MarkofArkay_ScrollChanceSlider_1	Scroll Chance
$mrt_MarkofArkay_LootChanceSlider_2	{0} Percent
$mrt_MarkofArkay_RPMinDistanceSlider_1	Min Distance to Respawn Point
$mrt_MarkofArkay_RPMinDistanceSlider_2	{0}
$mrt_MarkofArkay_NoFallDamageEnabled	No Fall Damage
$mrt_MarkofArkay_Effect	Enable Effect
$mrt_MarkofArkay_PotionRevivalEnabled	Enable Potion Revival
$mrt_MarkofArkay_AutoDrinkPotion	Auto Drink Potions
$mrt_MarkofArkay_RevivalRequireBlessing	Revival Requires Blessing
$mrt_MarkofArkay_ShiftBack	Shift Back Before Revival
$mrt_MarkofArkay_RecallRestriction	Put Limitation on Casting Recall Spell
$mrt_MarkofArkay_RecallCast	Recall Cost
$mrt_MarkofArkay_MarkCast	Mark Cost
$mrt_MarkofArkay_MenuEnabled	Enable Trade Menu
$mrt_MarkofArkay_EnableSave_M	Enable Save
$mrt_MarkofArkay_MarkPSlider	Priority of Arkay Mark
$mrt_MarkofArkay_GoldPSlider	Priority of Septim
$mrt_MarkofArkay_DragonSoulPSlider	Priority of Dragon Soul
$mrt_MarkofArkay_GSoulGemPSlider	Priority of Grand Soulgem
$mrt_MarkofArkay_BSoulGemPSlider	Priority of Black Soulgem
$mrt_MarkofArkay_TogglePowers1	Remove Powers
$mrt_MarkofArkay_TogglePowers2	Add Powers
$mrt_MarkofArkay_ToggleSpells1	Remove Spells
$mrt_MarkofArkay_ToggleSpells2	Add Spells
$mrt_MarkofArkay_NoTradingAftermath_M	Consequence of Not Trading
$mrt_MarkofArkay_TeleportLocation_M	Respawn Point
$mrt_MarkofArkay_RemovableItems_M	Lose
$mrt_MarkofArkay_RespawnNaked	Respawn Naked
$mrt_MarkofArkay_HealActors	Heal Others Before Respawn
$mrt_MarkofArkay_RespawnMenu	Enable Respawn Menu
$mrt_MarkofArkay_TeleportMenu	Enable Recall Menu
$mrt_MarkofArkay_Jail	Respawn in Prison
$mrt_MarkofArkay_ArkayCurse	Put Curse of Arkay on Player
$mrt_MarkofArkay_TempArkayCurse	Curse of Arkay is Temporary
$mrt_MarkofArkay_FollowerProtectPlayer	Enable Followers Protection
$mrt_MarkofArkay_ArkayCurses_M	Arkay's Curse
$mrt_MarkofArkay_AutoSwitchRP	Auto Switch Respawn Point
$mrt_MarkofArkay_Loseforever	Destroy Previously Lost Items
$mrt_MarkofArkay_SoulMarkStay	Soul Mark Remains in its Location
$mrt_MarkofArkay_NPCStealItems	Hostile NPCs Can Steal From the Player
$mrt_MarkofArkay_HostileNPC	Lose Items if a Hostile NPC is Nearby
$mrt_MarkofArkay_CreaturesCanSteal	Hostile Creatures Steal From the Player
$mrt_MarkofArkay_LostItemQuest	Track Lost Items with a Misc Quest
$mrt_MarkofArkay_ExternalTeleportLocation	External Respawn Point
$mrt_MarkofArkay_Status_Off	Start the Mod
$mrt_MarkofArkay_Status_On	Stop the Mod
$mrt_MarkofArkay_Status_Busy	Please Exit Menu
$mrt_MarkofArkay_Reset	Restart the Mod
$mrt_MarkofArkay_Notification	Enable Notifications
$mrt_MarkofArkay_Logging	Enable Logging
$mrt_MarkofArkay_FadeToBlack	Enable Black Screen
$mrt_MarkofArkay_Invisibility	Enable Invisibility Effect
$mrt_MarkofArkay_Info	Enable Information
$mrt_MarkofArkay_History	Enable History
$mrt_MarkofArkay_ResetHistory	Reset History
$mrt_MarkofArkay_Cur_Lost_Items	Currently Lost Items
$mrt_MarkofArkay_Revive_With_Potion	Revive by Auto Drinking Potion
$mrt_MarkofArkay_Revive_With_Revival_Spell	Revive with Auto Revival Spell
$mrt_MarkofArkay_Revive_With_Sacrifice_Spell	Revive with Sacrifice Spell
$mrt_MarkofArkay_Revive_By_Trading	Revive by Trading
$mrt_MarkofArkay_Revive_By_Follower	Revive by Follower
$mrt_MarkofArkay_Dis_From_Respawn	Distance from Respawn Point
$mrt_MarkofArkay_Destroyed_Items	Destroyed Items
$mrt_MarkofArkay_ResurrectActors	Resurrect Others Before Respawn
$mrt_MarkofArkay_Notification_Started	Mark of Arkay Started
$mrt_MarkofArkay_Notification_Stopped	Mark of Arkay Stopped
$mrt_MarkofArkay_Notification_Init	Arkay is watching
$mrt_MarkofArkay_Notification_Follower_In_Combat	Mark of Arkay - A Follower is still fighting...
$mrt_MarkofArkay_Notification_Follower_Avenged	Mark of Arkay - Your attacker is defeated...
$mrt_MarkofArkay_Notification_Revive_Potion	Mark of Arkay - You revived with healing potion.
$mrt_MarkofArkay_Notification_Revive_Revival_Scroll	Mark of Arkay - You revived yourself by using magic.
$mrt_MarkofArkay_Notification_Revive_Sacrifice_Scroll	Mark of Arkay - You revived yourself by sacrificing someone.
$mrt_MarkofArkay_Notification_RestoreLostItems	Mark of Arkay - Lost items recovered.
$mrt_MarkofArkay_Notification_SoulMark_Activated	Mark of Arkay - Soul mark activated.
$mrt_MarkofArkay_Notification_ArkayMark_Removed	Mark of Arkay - Arkay mark removed...
$mrt_MarkofArkay_Notification_BSoulGem_Removed	Mark of Arkay - Black soulgem removed...
$mrt_MarkofArkay_Notification_GSoulGem_Removed	Mark of Arkay - Grand soulgem removed...
$mrt_MarkofArkay_Notification_DragonSoul_Removed	Mark of Arkay - Dragon soul removed...
$mrt_MarkofArkay_Notification_Septim_Removed	Mark of Arkay - Septim removed...
$mrt_MarkofArkay_Notification_totalRemainingTrades	Mark of Arkay - Remaining trades base on your inventory...
$mrt_MarkofArkay_Notification_NoRemainingTrades	Mark of Arkay - You don't have enough items for another trade!
$mrt_MarkofArkay_Notification_Teleportation_Off	Teleportation is currently disabled.
$mrt_MarkofArkay_Notification_Teleportation_NoArkayMark	You don't have enough arkay marks.
$mrt_MarkofArkay_Notification_Sacrifice_Success	Mark of Arkay - Target marked for sacrifice.
$mrt_MarkofArkay_Notification_Sacrifice_Failed	Mark of Arkay - Target is too strong.
$mrt_MarkofArkay_LoseOptions_0	Nothing
$mrt_MarkofArkay_LoseOptions_1	Tradable Items
$mrt_MarkofArkay_LoseOptions_2	Unequipped Items + Tradables
$mrt_MarkofArkay_LoseOptions_3	Unequipped Items - Tradables
$mrt_MarkofArkay_LoseOptions_4	Everything - Tradables
$mrt_MarkofArkay_LoseOptions_5	Everything
$mrt_MarkofArkay_LoseOptions_6	Random - Everything
$mrt_MarkofArkay_LoseOptions_7	Random - Nothing
$mrt_MarkofArkay_LoseOptions_8	Random - Nothing & Everything
$mrt_MarkofArkay_LoseOptions_9	Random
$mrt_MarkofArkay_LoseOptions_10	Valuable
$mrt_MarkofArkay_LoseOptions_11	Valuables
$mrt_MarkofArkay_AftermathOptions_0	Death
$mrt_MarkofArkay_AftermathOptions_1	Respawn
$mrt_MarkofArkay_AftermathOptions_2	Main Menu
$mrt_MarkofArkay_ArkayCurses_0	Default
$mrt_MarkofArkay_ArkayCurses_1	Alternative
$mrt_MarkofArkay_ArkayCurses_2	Both
$mrt_MarkofArkay_SaveOptions_0	Always
$mrt_MarkofArkay_SaveOptions_1	Not in Bleedout
$mrt_MarkofArkay_SaveOptions_2	After Resting
$mrt_MarkofArkay_SaveOptions_3	After Praying to Arkay
$mrt_MarkofArkay_SaveOptions_4	After Both Rest and Pray
$mrt_MarkofArkay_DESC_RevivalEnabled_On	The player can be revived by trading items.\nIf revival is disabled, you can't trade any item but still, you can revive manually with healing potions. If you want to uninstall this mod, instead of disabling revival stop it from debug page.\nDefault: True
$mrt_MarkofArkay_DESC_RevivalEnabled_Off	If enabled, the player can be revived by trading items.\nIf revival is disabled, you can't trade any item but still, you can revive manually with healing potions. If you want to uninstall this mod, instead of disabling revival stop it from debug page.\nDefault: True
$mrt_MarkofArkay_DESC_DragonSoulSlider	How many dragon souls are needed for revival?\nDefault: 1.0
$mrt_MarkofArkay_DESC_MenuEnabled_On	A menu will appear asking what to do.\nDefault: True
$mrt_MarkofArkay_DESC_MenuEnabled_Off	Instead of showing a menu, items will be removed based on their priority.\nDefault: True
$mrt_MarkofArkay_DESC_BSoulGemSlider	How many filled black soul gems are needed for revival?\nDefault: 1.0
$mrt_MarkofArkay_DESC_GSoulGemSlider	How many filled grand soul gems are needed for revival?\nDefault: 1.0
$mrt_MarkofArkay_DESC_GoldSlider	How much gold is needed for revival?\nDefault: 1000.0
$mrt_MarkofArkay_DESC_MarkSlider	How many Arkay marks are needed for revival?\nDefault: 1.0
$mrt_MarkofArkay_DESC_RPMinDistanceSlider	When player's distance from the selected respawn point is lower than this value, another respawn point would be used.\nDefault: 2500.0
$mrt_MarkofArkay_DESC_DragonSoulRevivalEnabled_On	The player can be revived in exchange for dragon soul.\nDefault: True
$mrt_MarkofArkay_DESC_DragonSoulRevivalEnabled_Off	If enabled, the player can be revived in exchange for dragon soul.\nDefault: True
$mrt_MarkofArkay_DESC_MarkOfArkayRevivalEnabled_On	The player can be revived in exchange for Arkay mark.\nDefault: True
$mrt_MarkofArkay_DESC_MarkOfArkayRevivalEnabled_Off	If enabled, the player can be revived in exchange for Arkay mark.\nDefault: True
$mrt_MarkofArkay_DESC_BSoulGemRevivalEnabled_On	The player can be revived in exchange for filled black soul gem.\nDefault: True
$mrt_MarkofArkay_DESC_BSoulGemRevivalEnabled_Off	If enabled, the player can be revived in exchange for filled black soul gem.\nDefault: True
$mrt_MarkofArkay_DESC_GSoulGemRevivalEnabled_On	The player can be revived in exchange for filled grand soul gem.\nDefault: True
$mrt_MarkofArkay_DESC_GSoulGemRevivalEnabled_Off	If enabled, the player can be revived in exchange for filled grand soul gem.\nDefault: True
$mrt_MarkofArkay_DESC_GoldRevivalEnabled_On	The player can be revived in exchange for gold.\nDefault: True
$mrt_MarkofArkay_DESC_GoldRevivalEnabled_Off	If enabled, the player can be revived in exchange for gold.\nDefault: True
$mrt_MarkofArkay_DESC_NoFallDamageEnabled_On	The player won't take any damage from falling.(Requires SKSE)\nDefault: False
$mrt_MarkofArkay_DESC_NoFallDamageEnabled_Off	If enabled, The player won't take any damage from falling.(Requires SKSE)\nDefault: False
$mrt_MarkofArkay_DESC_Notification_On	Notifications about removed items, current revival and remaining revives are enabled.\nRemaining revives are calculated based on mod settings and items in inventory.\nDefault: False
$mrt_MarkofArkay_DESC_Notification_Off	Notifications about removed items, current revival and remaining revives are disabled.\nRemaining revives are calculated based on mod settings and items in inventory.\nDefault: False
$mrt_MarkofArkay_DESC_Logging	If enabled, mod events and variables will be recorded in Papyrus.0.log file.\nEnabling this option can impact game performance.\nDefault: False
$mrt_MarkofArkay_DESC_DragonSoulPSlider	Priority of dragon soul. When the menu is disabled, items with more priority will be used first.\nDefault: 2.0
$mrt_MarkofArkay_DESC_BSoulGemPSlider	Priority of filled black soul gems. When the menu is disabled, items with more priority will be used first.\nDefault: 1.0
$mrt_MarkofArkay_DESC_GSoulGemPSlider	Priority of filled grand soul gems. When the menu is disabled, items with more priority will be used first.\nDefault: 1.0
$mrt_MarkofArkay_DESC_MarkPSlider	Priority of Arkay mark. When the menu is disabled, items with more priority will be used first.\nDefault: 4.0
$mrt_MarkofArkay_DESC_GoldPSlider	Priority of gold. When the menu is disabled, items with more priority will be used first.\nDefault: 3.0
$mrt_MarkofArkay_DESC_RecoveryTime	How many seconds the mod will wait before reviving the player?\nDefault: 1.0
$mrt_MarkofArkay_DESC_BleedoutTime	How many seconds the mod will wait after entering bleedout?\nDefault: 6.0
$mrt_MarkofArkay_DESC_LootChanceSlider	Chance of finding Arkay Marks in the game. \nDefault: 50.0
$mrt_MarkofArkay_DESC_ScrollChanceSlider	Chance of finding Scrolls of revival and sacrifice in the game. \nDefault: 25.0
$mrt_MarkofArkay_DESC_Effect_On	While reviving an effect will be played which makes the player ethereal for a few seconds, also everytime that you load a savegame for three seconds the player won't take any damage.\nDefault: False
$mrt_MarkofArkay_DESC_Effect_Off	If enabled, while reviving an effect will be played which makes the player ethereal for a few seconds, also every time that you load a save game, for three seconds the player won't take any damage.
$mrt_MarkofArkay_DESC_RevivalRequireBlessing_On	The player can only be revived by trading items if an amulet of Arkay is equipped or have blessing by a shrine of Arkay.\nDefault: False
$mrt_MarkofArkay_DESC_RevivalRequireBlessing_Off	If enabled, player can only be revived by trading items if an amulet of Arkay is equipped or have the blessing of a shrine of Arkay.\nDefault: False
$mrt_MarkofArkay_DESC_PotionRevivalEnabled_On	When player's health drops to zero, you have few seconds to open the inventory and use healing potion for revival. \nDefault: False
$mrt_MarkofArkay_DESC_PotionRevivalEnabled_Off	If enabled, you can open the inventory and use healing potion for revival. \nDefault: False
$mrt_MarkofArkay_DESC_AutoDrinkPotion	If enabled, The player will be revived automatically with base game's healing potion in the inventory.\nDefault: False
$mrt_MarkofArkay_DESC_Status	Toggle for the mod. You should stop the mod before unistalling it.
$mrt_MarkofArkay_DESC_Reset	Restart the mod, clears custom respawn point and returns your lost items if they haven't been destroyed.
$mrt_MarkofArkay_DESC_NoTradingAftermath_M	Consequence of not having enough item or refusing to pay\n Death: The player will die. \n Respawn: The player will Respawn. \n Main menu: Game will quit to the main menu. \nDefault: Death
$mrt_MarkofArkay_DESC_TeleportLocation_M	The player will respawn in this Location. Nearby: The mod will try to find a respawwn point near current location. Slept Location: Near last slept location. Custom: Near a location which can be chosen with the Mark spell. External: If you add a reference to "ExternalMarkers" formlist, you will respawn near its location. If respawn fails or player is too close to the selected option, another one will be used. Default: Whiterun
$mrt_MarkofArkay_DESC_RespawnPoint0	If respawn fails or the selected respawn point is random, player would respawn in one of the enabled locations.\nAt least two locations must be enabled.\nDefault: True
$mrt_MarkofArkay_DESC_RespawnPoint1	If respawn fails or the selected respawn point is random, player would respawn in one of the enabled locations.\nAt least two locations must be enabled.\nDefault: True
$mrt_MarkofArkay_DESC_RespawnPoint2	If respawn fails or the selected respawn point is random, player would respawn in one of the enabled locations.\nAt least two locations must be enabled.\nDefault: True
$mrt_MarkofArkay_DESC_RespawnPoint3	If respawn fails or the selected respawn point is random, player would respawn in one of the enabled locations.\nAt least two locations must be enabled.\nDefault: True
$mrt_MarkofArkay_DESC_RespawnPoint4	If respawn fails or the selected respawn point is random, player would respawn in one of the enabled locations.\nAt least two locations must be enabled.\nDefault: True
$mrt_MarkofArkay_DESC_RespawnPoint5	If respawn fails or the selected respawn point is random, player would respawn in one of the enabled locations.\nAt least two locations must be enabled.\nDefault: True
$mrt_MarkofArkay_DESC_RespawnPoint6	If respawn fails or the selected respawn point is random, player would respawn in one of the enabled locations.\nAt least two locations must be enabled.\nDefault: True
$mrt_MarkofArkay_DESC_RespawnPoint7	If respawn fails or the selected respawn point is random, player would respawn in one of the enabled locations.\nAt least two locations must be enabled.\nDefault: True
$mrt_MarkofArkay_DESC_RespawnNaked	If enabled, the player will respawn naked. \nDefault: False
$mrt_MarkofArkay_DESC_RespawnMenu	If enabled, respawn point would be selected by answering a question. \nDefault: False
$mrt_MarkofArkay_DESC_TeleportMenu	If enabled, recall's destination would be selected by answering a question. \nDefault: False
$mrt_MarkofArkay_DESC_Jail	If enabled, Guards can send the player to jail. \nDefault: False 
$mrt_MarkofArkay_DESC_HealActors	If enabled, Last aggressor and everyone else who is near the player will be healed before respawn. \nDefault: False
$mrt_MarkofArkay_DESC_ResurrectActors	If enabled, Last aggressor and everyone else nearby who is not a unique NPC will be resurrected before respawn. \nDefault: False
$mrt_MarkofArkay_DESC_ArkayCurse	If enabled, the player will be cursed by Arkay. to remove the curse, activate the soul mark or retrieve your items or revive by trading or sacrificing someone.\nDefault: False
$mrt_MarkofArkay_DESC_TempArkayCurse	If enabled, Curse of arkay is temporary but you can't remove it by activating the soul mark. \nDefault: False
$mrt_MarkofArkay_DESC_RemovableItems_M	Before the respawn, player will lose something and a grand soulgem (Soul Mark) will be placed at the location. For recovering them, you need to activate the soul mark or sacrifice someone. Tradable Items: Enabled Items on the general page. Unequipped Items + Tradables: Tradables & everything in inventory - Equiped items. Everything: All - quest items. Random: One of previous options. Valuable: Dragon soul or any item with a base value of >=500. Default: Nothing
$mrt_MarkofArkay_DESC_Loseforever	If enabled and player respawn before retrieving lost items, They will be lost permanently.\nDefault: False
$mrt_MarkofArkay_DESC_LostItemQuest	If enabled you can track your lost items with a quest in your journal.\nDefault: True
$mrt_MarkofArkay_DESC_SoulMarkStay	If enabled and you respawn before retrieving your items, soul mark will remain in its location. \nDefault: False
$mrt_MarkofArkay_DESC_RecallRestriction	If enabled, You can't cast recall spell when fast travel is disabled.\nDefault: True
$mrt_MarkofArkay_DESC_ExternalTeleportLocation	If you add markers to "zzzmoa_ExternalMarkers" formlist and select external as respawn point, you will respawn near the marker with this index number in the formlist.\nDefault: Random
$mrt_MarkofArkay_DESC_ShiftBack	If enabled, The Player will shift back from beast forms before revival.\nDefault: False
$mrt_MarkofArkay_DESC_History	If enabled, History of player's interactions with the mod will be displayed.\nDefault: False
$mrt_MarkofArkay_DESC_Info	If enabled, History of player's interactions with the mod, Number of lost items and distance from selected respawn point will be displayed. For each type of items only one will be counted, for example 5 gold ingot and 10 septims will be displayed as two items not 15.\nDefault: False
$mrt_MarkofArkay_DESC_ResetHistory	Clears the history of player's interactions with the mod.
$mrt_MarkofArkay_DESC_AutoSwitchRP	If enabled, Respawn point will be switched to "Custom" on casting Mark spell and "Slept Location" after sleeping.\nDefault: False
$mrt_MarkofArkay_ShiftBackRespawn	Shift Back Before Respawn
$mrt_MarkofArkay_DESC_ShiftBackRespawn	If enabled, player will shift back from beast forms before respawning. If disabled and player is in beast form, nothing will be removed from inventory before respawning. \nDefault: True
$mrt_MarkofArkay_DESC_RecallCost	How many arkay marks are needed for casting the recall spell?\nDefault: 0.0
$mrt_MarkofArkay_DESC_MarkCost	How many arkay marks are needed for casting the mark spell?\nDefault: 0.0
$mrt_MarkofArkay_DESC_ArkayCurses_M	Default: (Prices: 50% worse, Persuasion and intimidation: 50% harder)\nAlternative: (Health,Stamina,Magicka: 25 points lower)
$mrt_MarkofArkay_DESC_TogglePowers	Adds or removes Revival and Sacrifice powers from the player.
$mrt_MarkofArkay_DESC_ToggleSpells	Adds or removes Mark and Recall spells from the player.
$mrt_MarkofArkay_DESC_EnableSave_M	When you should be able to save the game?\nDefault: Out of Bleedout
$mrt_MarkofArkay_DESC_FollowerProtectPlayer	If enabled, followers would revive and respawn with the player and the player would remain in battle, if at least one follower is still fighting or the last NPC who attacked the player is defeated. \nDefault: False
$mrt_MarkofArkay_DESC_NPCStealItems	If enabled, the mod will place your items in a hostile NPCs Inventoy, if there is no hostile NPC in your location, you won't lose any item. If arkay's curse is enabled but no NPC is around then the soul mark would still be placed in your location. \n Default: False
$mrt_MarkofArkay_DESC_CreaturesCanSteal	If enabled, some creatures can steal from the player.\nDefault: False
$mrt_MarkofArkay_DESC_HostileNPC	If enabled, you only lose your items when a hostile NPC who can steal your items is nearby. You won't lose any items if there is no hostile NPC in your location. If arkay's curse is enabled but no NPC is around then the soul mark would still be placed in your location. \n Default: False
$mrt_MarkofArkay_DESC_FadeToBlack	If enabled, the screen will fade to black before respawning.\nDefault: True
$mrt_MarkofArkay_DESC_Invisibility	If enabled, the player will become invisible before respawning.\nDefault: False